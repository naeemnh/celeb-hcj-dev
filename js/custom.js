$(window).on('load', function () {
	// Animation using ScrollReveal
	// Selecting objects to be animated
	var animatedList1 =
		'.container > h2, h2 ~ p, about-page h6, .about-page h3, .about-page p, .about-page h6';

	var animatedList2 = '.snip1432, .thumbnail, .block-lg, .button, .about-page';

	var animatedList3 = 'figcaption h1, ficaption h2';

	var animatedList4 = '.client';

	// Every list will have different animation
	ScrollReveal().reveal(animatedList1, {
		duration: 800,
		distance: '50px',
		interval: 100,
	});
	ScrollReveal().reveal(animatedList2, {
		duration: 800,
		scale: 0.8,
		distance: '50px',
		interval: 50,
	});
	ScrollReveal().reveal(animatedList3, { duration: 1000, interval: 100 });
	ScrollReveal().reveal(animatedList4, { duration: 800, interval: 50 });

	$('.tabs .row .col-md-3:first button').addClass('active');
	$('.services').hide();
	$('.services:first').show();

	$('.tabs .row .col-md-3 button').on('click', function () {
		var i = $(this).data('link');
		console.log(i);
		// setting the clicked option as active
		if (!$(this).hasClass('active')) {
			$('.tabs .row .col-md-3 button').removeClass('active');
			$(this).addClass('active');
		}
		$('.services').hide();
		$('#' + i).fadeIn('slow');
		$('#' + i).show();
		// $('#' + i + ' .row celeb-color-div').show();
	});
});
